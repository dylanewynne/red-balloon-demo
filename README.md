# Frontend Code Project

## Purpose
This project is meant to test an applicant's ability to implement frontend web technologies. The project is designed to be completed in less than four hours, although you may take more time if you would like.

## Project Description
You need to create a front-end web app that displays a list of job postings. You will query an endpoint [here](https://run.mocky.io/v3/3cbadbe0-009c-4239-ac4f-e52068d2cd48?mocky-delay=2000ms) to receive the data to display. The endpoint will return a JSON object with an array of job postings, and your app should display the data in a way that resembles the mock in the image found [here](https://i.imgur.com/gY526Ju.mp4).

The endpoint has a 2 second delay before it returns a response. To make the user experience more seamless, you *need* to add a loading state while the fetch request is loading. The loading state should display a spinner, text, or any other loading animation of your choice. 

## Implementation Notes
- To complete the challenge, you can use any frontend framework or library of your choice, such as React, Angular, Vue, or plain JavaScript. 
- Provide instructions on how to run/test your code, including any instructions for required packages.
- If you are unsure about any of the project requirements, use your best judgement and be prepared to defend your decision.
- Make sure not to display the postings statically, but **query the endpoint** to get the data.
- Your app does not have to perfectly match the mock, but try your best. (For example, the card animation when you hover is not critical but would be great to see)
- As mentioned before, the url to get the job posting data can be found here: https://run.mocky.io/v3/3cbadbe0-009c-4239-ac4f-e52068d2cd48?mocky-delay=2000ms and the mock can be found here: https://i.imgur.com/gY526Ju.mp4

## Submission
- Submit your solution by creating a Gitlab repository (you can fork this repo) and email us the link. 
- Your repository should contain a README with a concise description of your project and instructions on how to run your code.

## Code Review
After you submit your solution, we will schedule a code review interview with you. You should be prepared to:
- Give a live demonstration of your solution
- Explain what the code is doing
- Defend your design decisions 
- Answer questions about libraries you chose to use
- Discuss alternate solutions. What other solutions did you consider? Why did you choose your approach? 